package com.cothink.judge.grpc;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.0.0)",
    comments = "Source: oj.proto")
public class OnlineJudgeGrpc {

  private OnlineJudgeGrpc() {}

  public static final String SERVICE_NAME = "com.cothink.judge.grpc.OnlineJudge";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<com.cothink.judge.grpc.CommitJudge,
      com.cothink.judge.grpc.JudgeMessage> METHOD_SEND_JUDGE =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "com.cothink.judge.grpc.OnlineJudge", "SendJudge"),
          io.grpc.protobuf.ProtoUtils.marshaller(com.cothink.judge.grpc.CommitJudge.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(com.cothink.judge.grpc.JudgeMessage.getDefaultInstance()));

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static OnlineJudgeStub newStub(io.grpc.Channel channel) {
    return new OnlineJudgeStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static OnlineJudgeBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new OnlineJudgeBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary and streaming output calls on the service
   */
  public static OnlineJudgeFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new OnlineJudgeFutureStub(channel);
  }

  /**
   */
  public static abstract class OnlineJudgeImplBase implements io.grpc.BindableService {

    /**
     */
    public void sendJudge(com.cothink.judge.grpc.CommitJudge request,
        io.grpc.stub.StreamObserver<com.cothink.judge.grpc.JudgeMessage> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_SEND_JUDGE, responseObserver);
    }

    @java.lang.Override public io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_SEND_JUDGE,
            asyncUnaryCall(
              new MethodHandlers<
                com.cothink.judge.grpc.CommitJudge,
                com.cothink.judge.grpc.JudgeMessage>(
                  this, METHODID_SEND_JUDGE)))
          .build();
    }
  }

  /**
   */
  public static final class OnlineJudgeStub extends io.grpc.stub.AbstractStub<OnlineJudgeStub> {
    private OnlineJudgeStub(io.grpc.Channel channel) {
      super(channel);
    }

    private OnlineJudgeStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected OnlineJudgeStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new OnlineJudgeStub(channel, callOptions);
    }

    /**
     */
    public void sendJudge(com.cothink.judge.grpc.CommitJudge request,
        io.grpc.stub.StreamObserver<com.cothink.judge.grpc.JudgeMessage> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_SEND_JUDGE, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class OnlineJudgeBlockingStub extends io.grpc.stub.AbstractStub<OnlineJudgeBlockingStub> {
    private OnlineJudgeBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private OnlineJudgeBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected OnlineJudgeBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new OnlineJudgeBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.cothink.judge.grpc.JudgeMessage sendJudge(com.cothink.judge.grpc.CommitJudge request) {
      return blockingUnaryCall(
          getChannel(), METHOD_SEND_JUDGE, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class OnlineJudgeFutureStub extends io.grpc.stub.AbstractStub<OnlineJudgeFutureStub> {
    private OnlineJudgeFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private OnlineJudgeFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected OnlineJudgeFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new OnlineJudgeFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.cothink.judge.grpc.JudgeMessage> sendJudge(
        com.cothink.judge.grpc.CommitJudge request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_SEND_JUDGE, getCallOptions()), request);
    }
  }

  private static final int METHODID_SEND_JUDGE = 0;

  private static class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final OnlineJudgeImplBase serviceImpl;
    private final int methodId;

    public MethodHandlers(OnlineJudgeImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SEND_JUDGE:
          serviceImpl.sendJudge((com.cothink.judge.grpc.CommitJudge) request,
              (io.grpc.stub.StreamObserver<com.cothink.judge.grpc.JudgeMessage>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    return new io.grpc.ServiceDescriptor(SERVICE_NAME,
        METHOD_SEND_JUDGE);
  }

}
