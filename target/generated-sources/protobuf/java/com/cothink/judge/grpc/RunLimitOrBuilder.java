// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: oj.proto

package com.cothink.judge.grpc;

public interface RunLimitOrBuilder extends
    // @@protoc_insertion_point(interface_extends:com.cothink.judge.grpc.RunLimit)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   *运行时间限制
   * </pre>
   *
   * <code>optional uint64 timeoutLimit = 1;</code>
   */
  long getTimeoutLimit();

  /**
   * <pre>
   *内存大小限制
   * </pre>
   *
   * <code>optional uint32 memoryLimit = 2;</code>
   */
  int getMemoryLimit();
}
