// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: oj.proto

package com.cothink.judge.grpc;

/**
 * Protobuf type {@code com.cothink.judge.grpc.JudgeMessage}
 */
public  final class JudgeMessage extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:com.cothink.judge.grpc.JudgeMessage)
    JudgeMessageOrBuilder {
  // Use JudgeMessage.newBuilder() to construct.
  private JudgeMessage(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private JudgeMessage() {
    status_ = 0;
    result_ = "";
    total_ = 0;
    passed_ = 0;
    passRate_ = 0D;
    time_ = 0L;
    memory_ = 0L;
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
  }
  private JudgeMessage(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!input.skipField(tag)) {
              done = true;
            }
            break;
          }
          case 8: {
            int rawValue = input.readEnum();

            status_ = rawValue;
            break;
          }
          case 18: {
            java.lang.String s = input.readStringRequireUtf8();

            result_ = s;
            break;
          }
          case 24: {

            total_ = input.readUInt32();
            break;
          }
          case 32: {

            passed_ = input.readUInt32();
            break;
          }
          case 41: {

            passRate_ = input.readDouble();
            break;
          }
          case 48: {

            time_ = input.readUInt64();
            break;
          }
          case 56: {

            memory_ = input.readUInt64();
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return com.cothink.judge.grpc.CommitJudgeGrpcProto.internal_static_com_cothink_judge_grpc_JudgeMessage_descriptor;
  }

  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return com.cothink.judge.grpc.CommitJudgeGrpcProto.internal_static_com_cothink_judge_grpc_JudgeMessage_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            com.cothink.judge.grpc.JudgeMessage.class, com.cothink.judge.grpc.JudgeMessage.Builder.class);
  }

  public static final int STATUS_FIELD_NUMBER = 1;
  private int status_;
  /**
   * <pre>
   *结果状态
   * </pre>
   *
   * <code>optional .com.cothink.judge.grpc.Status status = 1;</code>
   */
  public int getStatusValue() {
    return status_;
  }
  /**
   * <pre>
   *结果状态
   * </pre>
   *
   * <code>optional .com.cothink.judge.grpc.Status status = 1;</code>
   */
  public com.cothink.judge.grpc.Status getStatus() {
    com.cothink.judge.grpc.Status result = com.cothink.judge.grpc.Status.valueOf(status_);
    return result == null ? com.cothink.judge.grpc.Status.UNRECOGNIZED : result;
  }

  public static final int RESULT_FIELD_NUMBER = 2;
  private volatile java.lang.Object result_;
  /**
   * <code>optional string result = 2;</code>
   */
  public java.lang.String getResult() {
    java.lang.Object ref = result_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      result_ = s;
      return s;
    }
  }
  /**
   * <code>optional string result = 2;</code>
   */
  public com.google.protobuf.ByteString
      getResultBytes() {
    java.lang.Object ref = result_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      result_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int TOTAL_FIELD_NUMBER = 3;
  private int total_;
  /**
   * <code>optional uint32 total = 3;</code>
   */
  public int getTotal() {
    return total_;
  }

  public static final int PASSED_FIELD_NUMBER = 4;
  private int passed_;
  /**
   * <code>optional uint32 passed = 4;</code>
   */
  public int getPassed() {
    return passed_;
  }

  public static final int PASSRATE_FIELD_NUMBER = 5;
  private double passRate_;
  /**
   * <code>optional double passRate = 5;</code>
   */
  public double getPassRate() {
    return passRate_;
  }

  public static final int TIME_FIELD_NUMBER = 6;
  private long time_;
  /**
   * <code>optional uint64 time = 6;</code>
   */
  public long getTime() {
    return time_;
  }

  public static final int MEMORY_FIELD_NUMBER = 7;
  private long memory_;
  /**
   * <code>optional uint64 memory = 7;</code>
   */
  public long getMemory() {
    return memory_;
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (status_ != com.cothink.judge.grpc.Status.UNKNOWN.getNumber()) {
      output.writeEnum(1, status_);
    }
    if (!getResultBytes().isEmpty()) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 2, result_);
    }
    if (total_ != 0) {
      output.writeUInt32(3, total_);
    }
    if (passed_ != 0) {
      output.writeUInt32(4, passed_);
    }
    if (passRate_ != 0D) {
      output.writeDouble(5, passRate_);
    }
    if (time_ != 0L) {
      output.writeUInt64(6, time_);
    }
    if (memory_ != 0L) {
      output.writeUInt64(7, memory_);
    }
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (status_ != com.cothink.judge.grpc.Status.UNKNOWN.getNumber()) {
      size += com.google.protobuf.CodedOutputStream
        .computeEnumSize(1, status_);
    }
    if (!getResultBytes().isEmpty()) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(2, result_);
    }
    if (total_ != 0) {
      size += com.google.protobuf.CodedOutputStream
        .computeUInt32Size(3, total_);
    }
    if (passed_ != 0) {
      size += com.google.protobuf.CodedOutputStream
        .computeUInt32Size(4, passed_);
    }
    if (passRate_ != 0D) {
      size += com.google.protobuf.CodedOutputStream
        .computeDoubleSize(5, passRate_);
    }
    if (time_ != 0L) {
      size += com.google.protobuf.CodedOutputStream
        .computeUInt64Size(6, time_);
    }
    if (memory_ != 0L) {
      size += com.google.protobuf.CodedOutputStream
        .computeUInt64Size(7, memory_);
    }
    memoizedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof com.cothink.judge.grpc.JudgeMessage)) {
      return super.equals(obj);
    }
    com.cothink.judge.grpc.JudgeMessage other = (com.cothink.judge.grpc.JudgeMessage) obj;

    boolean result = true;
    result = result && status_ == other.status_;
    result = result && getResult()
        .equals(other.getResult());
    result = result && (getTotal()
        == other.getTotal());
    result = result && (getPassed()
        == other.getPassed());
    result = result && (
        java.lang.Double.doubleToLongBits(getPassRate())
        == java.lang.Double.doubleToLongBits(
            other.getPassRate()));
    result = result && (getTime()
        == other.getTime());
    result = result && (getMemory()
        == other.getMemory());
    return result;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptorForType().hashCode();
    hash = (37 * hash) + STATUS_FIELD_NUMBER;
    hash = (53 * hash) + status_;
    hash = (37 * hash) + RESULT_FIELD_NUMBER;
    hash = (53 * hash) + getResult().hashCode();
    hash = (37 * hash) + TOTAL_FIELD_NUMBER;
    hash = (53 * hash) + getTotal();
    hash = (37 * hash) + PASSED_FIELD_NUMBER;
    hash = (53 * hash) + getPassed();
    hash = (37 * hash) + PASSRATE_FIELD_NUMBER;
    hash = (53 * hash) + com.google.protobuf.Internal.hashLong(
        java.lang.Double.doubleToLongBits(getPassRate()));
    hash = (37 * hash) + TIME_FIELD_NUMBER;
    hash = (53 * hash) + com.google.protobuf.Internal.hashLong(
        getTime());
    hash = (37 * hash) + MEMORY_FIELD_NUMBER;
    hash = (53 * hash) + com.google.protobuf.Internal.hashLong(
        getMemory());
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static com.cothink.judge.grpc.JudgeMessage parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.cothink.judge.grpc.JudgeMessage parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.cothink.judge.grpc.JudgeMessage parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.cothink.judge.grpc.JudgeMessage parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.cothink.judge.grpc.JudgeMessage parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.cothink.judge.grpc.JudgeMessage parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.cothink.judge.grpc.JudgeMessage parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static com.cothink.judge.grpc.JudgeMessage parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.cothink.judge.grpc.JudgeMessage parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.cothink.judge.grpc.JudgeMessage parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(com.cothink.judge.grpc.JudgeMessage prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code com.cothink.judge.grpc.JudgeMessage}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:com.cothink.judge.grpc.JudgeMessage)
      com.cothink.judge.grpc.JudgeMessageOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.cothink.judge.grpc.CommitJudgeGrpcProto.internal_static_com_cothink_judge_grpc_JudgeMessage_descriptor;
    }

    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.cothink.judge.grpc.CommitJudgeGrpcProto.internal_static_com_cothink_judge_grpc_JudgeMessage_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.cothink.judge.grpc.JudgeMessage.class, com.cothink.judge.grpc.JudgeMessage.Builder.class);
    }

    // Construct using com.cothink.judge.grpc.JudgeMessage.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    public Builder clear() {
      super.clear();
      status_ = 0;

      result_ = "";

      total_ = 0;

      passed_ = 0;

      passRate_ = 0D;

      time_ = 0L;

      memory_ = 0L;

      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return com.cothink.judge.grpc.CommitJudgeGrpcProto.internal_static_com_cothink_judge_grpc_JudgeMessage_descriptor;
    }

    public com.cothink.judge.grpc.JudgeMessage getDefaultInstanceForType() {
      return com.cothink.judge.grpc.JudgeMessage.getDefaultInstance();
    }

    public com.cothink.judge.grpc.JudgeMessage build() {
      com.cothink.judge.grpc.JudgeMessage result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public com.cothink.judge.grpc.JudgeMessage buildPartial() {
      com.cothink.judge.grpc.JudgeMessage result = new com.cothink.judge.grpc.JudgeMessage(this);
      result.status_ = status_;
      result.result_ = result_;
      result.total_ = total_;
      result.passed_ = passed_;
      result.passRate_ = passRate_;
      result.time_ = time_;
      result.memory_ = memory_;
      onBuilt();
      return result;
    }

    public Builder clone() {
      return (Builder) super.clone();
    }
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.setField(field, value);
    }
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return (Builder) super.clearField(field);
    }
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return (Builder) super.clearOneof(oneof);
    }
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, Object value) {
      return (Builder) super.setRepeatedField(field, index, value);
    }
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.addRepeatedField(field, value);
    }
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof com.cothink.judge.grpc.JudgeMessage) {
        return mergeFrom((com.cothink.judge.grpc.JudgeMessage)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(com.cothink.judge.grpc.JudgeMessage other) {
      if (other == com.cothink.judge.grpc.JudgeMessage.getDefaultInstance()) return this;
      if (other.status_ != 0) {
        setStatusValue(other.getStatusValue());
      }
      if (!other.getResult().isEmpty()) {
        result_ = other.result_;
        onChanged();
      }
      if (other.getTotal() != 0) {
        setTotal(other.getTotal());
      }
      if (other.getPassed() != 0) {
        setPassed(other.getPassed());
      }
      if (other.getPassRate() != 0D) {
        setPassRate(other.getPassRate());
      }
      if (other.getTime() != 0L) {
        setTime(other.getTime());
      }
      if (other.getMemory() != 0L) {
        setMemory(other.getMemory());
      }
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      com.cothink.judge.grpc.JudgeMessage parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (com.cothink.judge.grpc.JudgeMessage) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private int status_ = 0;
    /**
     * <pre>
     *结果状态
     * </pre>
     *
     * <code>optional .com.cothink.judge.grpc.Status status = 1;</code>
     */
    public int getStatusValue() {
      return status_;
    }
    /**
     * <pre>
     *结果状态
     * </pre>
     *
     * <code>optional .com.cothink.judge.grpc.Status status = 1;</code>
     */
    public Builder setStatusValue(int value) {
      status_ = value;
      onChanged();
      return this;
    }
    /**
     * <pre>
     *结果状态
     * </pre>
     *
     * <code>optional .com.cothink.judge.grpc.Status status = 1;</code>
     */
    public com.cothink.judge.grpc.Status getStatus() {
      com.cothink.judge.grpc.Status result = com.cothink.judge.grpc.Status.valueOf(status_);
      return result == null ? com.cothink.judge.grpc.Status.UNRECOGNIZED : result;
    }
    /**
     * <pre>
     *结果状态
     * </pre>
     *
     * <code>optional .com.cothink.judge.grpc.Status status = 1;</code>
     */
    public Builder setStatus(com.cothink.judge.grpc.Status value) {
      if (value == null) {
        throw new NullPointerException();
      }
      
      status_ = value.getNumber();
      onChanged();
      return this;
    }
    /**
     * <pre>
     *结果状态
     * </pre>
     *
     * <code>optional .com.cothink.judge.grpc.Status status = 1;</code>
     */
    public Builder clearStatus() {
      
      status_ = 0;
      onChanged();
      return this;
    }

    private java.lang.Object result_ = "";
    /**
     * <code>optional string result = 2;</code>
     */
    public java.lang.String getResult() {
      java.lang.Object ref = result_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        result_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>optional string result = 2;</code>
     */
    public com.google.protobuf.ByteString
        getResultBytes() {
      java.lang.Object ref = result_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        result_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>optional string result = 2;</code>
     */
    public Builder setResult(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      result_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional string result = 2;</code>
     */
    public Builder clearResult() {
      
      result_ = getDefaultInstance().getResult();
      onChanged();
      return this;
    }
    /**
     * <code>optional string result = 2;</code>
     */
    public Builder setResultBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      result_ = value;
      onChanged();
      return this;
    }

    private int total_ ;
    /**
     * <code>optional uint32 total = 3;</code>
     */
    public int getTotal() {
      return total_;
    }
    /**
     * <code>optional uint32 total = 3;</code>
     */
    public Builder setTotal(int value) {
      
      total_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional uint32 total = 3;</code>
     */
    public Builder clearTotal() {
      
      total_ = 0;
      onChanged();
      return this;
    }

    private int passed_ ;
    /**
     * <code>optional uint32 passed = 4;</code>
     */
    public int getPassed() {
      return passed_;
    }
    /**
     * <code>optional uint32 passed = 4;</code>
     */
    public Builder setPassed(int value) {
      
      passed_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional uint32 passed = 4;</code>
     */
    public Builder clearPassed() {
      
      passed_ = 0;
      onChanged();
      return this;
    }

    private double passRate_ ;
    /**
     * <code>optional double passRate = 5;</code>
     */
    public double getPassRate() {
      return passRate_;
    }
    /**
     * <code>optional double passRate = 5;</code>
     */
    public Builder setPassRate(double value) {
      
      passRate_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional double passRate = 5;</code>
     */
    public Builder clearPassRate() {
      
      passRate_ = 0D;
      onChanged();
      return this;
    }

    private long time_ ;
    /**
     * <code>optional uint64 time = 6;</code>
     */
    public long getTime() {
      return time_;
    }
    /**
     * <code>optional uint64 time = 6;</code>
     */
    public Builder setTime(long value) {
      
      time_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional uint64 time = 6;</code>
     */
    public Builder clearTime() {
      
      time_ = 0L;
      onChanged();
      return this;
    }

    private long memory_ ;
    /**
     * <code>optional uint64 memory = 7;</code>
     */
    public long getMemory() {
      return memory_;
    }
    /**
     * <code>optional uint64 memory = 7;</code>
     */
    public Builder setMemory(long value) {
      
      memory_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional uint64 memory = 7;</code>
     */
    public Builder clearMemory() {
      
      memory_ = 0L;
      onChanged();
      return this;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }


    // @@protoc_insertion_point(builder_scope:com.cothink.judge.grpc.JudgeMessage)
  }

  // @@protoc_insertion_point(class_scope:com.cothink.judge.grpc.JudgeMessage)
  private static final com.cothink.judge.grpc.JudgeMessage DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new com.cothink.judge.grpc.JudgeMessage();
  }

  public static com.cothink.judge.grpc.JudgeMessage getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<JudgeMessage>
      PARSER = new com.google.protobuf.AbstractParser<JudgeMessage>() {
    public JudgeMessage parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new JudgeMessage(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<JudgeMessage> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<JudgeMessage> getParserForType() {
    return PARSER;
  }

  public com.cothink.judge.grpc.JudgeMessage getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

