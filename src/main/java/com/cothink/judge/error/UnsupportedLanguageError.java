package com.cothink.judge.error;

public class UnsupportedLanguageError extends Exception {
    public UnsupportedLanguageError(String msg) {
        super(msg);
    }
}
