package com.cothink.judge.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Slf4j
@Component
public class FileWriteUtil {
    public void write(File file, String content) throws IOException {
        FileWriter fileWriter = null;
        try {
             fileWriter = new FileWriter(file);
             fileWriter.write(content);
             fileWriter.flush();
        } catch (IOException e) {
            log.info("文件写入失败:{}",file);
        }finally {
            if(fileWriter != null){
                fileWriter.close();
            }
        }

    }
}
