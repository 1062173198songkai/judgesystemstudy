package com.cothink.judge.model;

import com.cothink.judge.type.SolutionResult;
import com.cothink.judge.type.SolutionState;
import lombok.Data;

import java.sql.Date;

@Data
public class JudgeResult {
    private String solutionId;
    private Integer problemId;
    private String userId;
    private Integer language;
    private SolutionState state;
    private SolutionResult result;
    private Integer compileState;
    private String compileInfo;
    private double passRate;
    private double score;
    private Long time;
    private Long memory;
    private String errorInfo;
    private Date submitTime;
}
