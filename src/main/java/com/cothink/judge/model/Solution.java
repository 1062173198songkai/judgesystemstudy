package com.cothink.judge.model;

import com.cothink.judge.type.SolutionResult;
import com.cothink.judge.type.SolutionState;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class Solution implements Serializable {
    private String solutionId;
    private Integer problemId;
    private Integer contestId;
    private Integer language;
    private SolutionState state;
    private SolutionResult result;
    private double passRate;
    private String sourceCode;
    //0是编程题 1是SQL
    private Integer type;
    //正确SQL 当且仅当是编程题时有用
    private String correctSQL;
    //测试数据和答案 当且仅当是编程题时有用
    private Map<String,String> testData;
    private RunResult runResult;
    //SQL类
    private Runtime runtime;

    /**
     * 编程题构造函数
     * @param solutionId
     * @param language
     * @param type
     * @param testData
     */
    public Solution(String solutionId, Integer language, int type,Map<String,String> testData,String sourceCode) {
        this.solutionId = solutionId;
        this.language = language;
        this.type = type;
        this.testData = testData;
        this.sourceCode = sourceCode;
    }

    /**
     * SQL题编程
     * @param solutionId
     * @param language
     * @param type
     * @param correctSQL
     */
    public Solution(String solutionId, Integer language, int type,String correctSQL) {
        this.solutionId = solutionId;
        this.language = language;
        this.type = type;
        this.correctSQL = correctSQL;
    }
}
