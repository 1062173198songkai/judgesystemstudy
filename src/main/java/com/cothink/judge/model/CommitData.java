package com.cothink.judge.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class CommitData implements Serializable {
    private String solutionId;
    private String userId;
    private Integer problemId;
    private Integer contestId;
    private String sourceCode;
    private Integer language;
    //是编程题还是SQL题目
    private Integer type;
}
