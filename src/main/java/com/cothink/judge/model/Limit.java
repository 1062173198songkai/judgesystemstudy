package com.cothink.judge.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Limit {
    //运行限制时间 s
    private long timeout;
    //内存限制 kb
    private int memoryLimit;
}
