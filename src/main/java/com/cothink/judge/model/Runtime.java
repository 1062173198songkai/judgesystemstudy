package com.cothink.judge.model;

import com.cothink.judge.type.SolutionResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * 编程题目运行类
 * @author SONGKAI
 *
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Runtime {
    private Integer id;
    private String solutionId;
    private Integer total;
    private Integer passed;
    private Long time;
    private Long memory;
    private SolutionResult result;
    private String info;

    public Runtime(String solutionId) {
        this(null, solutionId, 0, 0, null, null, null, null);
    }
}
