package com.cothink.judge.service.Imp;

import com.cothink.judge.componet.JudgementAsync;
import com.cothink.judge.grpc.CommitJudge;
import com.cothink.judge.grpc.JudgeMessage;
import com.cothink.judge.grpc.OnlineJudgeGrpc;
import com.cothink.judge.grpc.Status;
import com.cothink.judge.model.Solution;
import com.cothink.judge.service.JudgeService;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

@Service
@Slf4j
public class JudgeServiceImp extends OnlineJudgeGrpc.OnlineJudgeImplBase implements JudgeService {

    @Autowired
    JudgementAsync judgementAsync;
    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    @Override
    public void sendJudge(CommitJudge request,
                          StreamObserver<JudgeMessage> responseObserver) {
        log.info("commitJudge:{}",request);
        long startTime = System.currentTimeMillis();
        Future<Solution> solutionFuture = judgementAsync.judge(request,(Solution solution) -> {
            long endTime = System.currentTimeMillis();
            log.info("solution:{},开始时间:{},结束时间:{},耗时:{}",solution,startTime,endTime,endTime-startTime);
            //配置数据库后 加载落库操作
        });
        Solution solution = null;
        JudgeMessage judgeMessage = null;
        try {
            while(!solutionFuture.isDone()){
                Thread.sleep(10);
            }
            if(solutionFuture.isDone()){
                solution = solutionFuture.get();
            }
            if (solution.getType() == 0) {
                //编程题
                log.info("solution:{}",solution);
                judgeMessage = JudgeMessage.newBuilder().setStatus(Status.forNumber(solution.getRunResult().getStatus())).
                        setMemory(solution.getRunResult().getMemory()).setTotal(solution.getRunResult().getTotal()).
                        setPassed(solution.getRunResult().getPassed()).setPassRate(solution.getPassRate()).
                        setTime(solution.getRunResult().getTime()).buildPartial();
            } else {
                //SQL题目
//                judgeMessage = JudgeMessage.newBuilder().setStatus(Status.forNumber(solution.getState().value()))
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            log.error("获取结果失败",e);
            judgeMessage = JudgeMessage.newBuilder().buildPartial();
        }
        responseObserver.onNext(judgeMessage);
        responseObserver.onCompleted();
    }
}
