package com.cothink.judge.server;

import com.cothink.judge.service.Imp.JudgeServiceImp;
import com.cothink.judge.service.JudgeService;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.InetAddress;

/**
 * 自动判定
 */
@Slf4j
@Service
public class JudgeServer {

    private String port;

    private Server server;

    public JudgeServer(Environment environment, JudgeServiceImp judgeService) throws IOException {
        port = environment.getProperty("judge-port");
        log.info("port:{}",port);
        start(judgeService);
    }

    public void start(JudgeServiceImp judgeService) throws IOException {
        log.info("ip:{},端口号:{}", InetAddress.getLocalHost().getHostAddress(),port,server);
        server = ServerBuilder.forPort(Integer.valueOf(port)).addService(judgeService).build().start();
        log.info("服务启动");
        log.info("ip:{},端口号:{},server:{}", InetAddress.getLocalHost().getHostAddress(),port,server);
        Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
            public void run(){
                log.error("------shutting down gRPC server since JVM is shutting down-------");
                JudgeServer.this.stop();
                log.error("自动判题服务关闭");
            }
        });
    }
    public void stop(){
        if(server !=  null){
            server.shutdown();
        }
    }
    public void blockUntilShutdown() throws InterruptedException {
        if(server != null){
            server.awaitTermination();
        }
    }
}
