package com.cothink.judge.type;

public interface BaseEnum {
    int getValue();
}
