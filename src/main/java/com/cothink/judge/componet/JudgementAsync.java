package com.cothink.judge.componet;

import com.cothink.judge.grpc.CommitJudge;
import com.cothink.judge.model.Solution;
import com.cothink.judge.type.SolutionResult;
import com.cothink.judge.type.SolutionState;
import com.cothink.judge.utils.FileCleaner;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Future;
import java.util.function.Consumer;

@Slf4j
@Component
public class JudgementAsync {

    @Resource
    private Judgement judgement;

    @Resource
    private SqlJudgement sqlJudgement;


    @Resource
    private FileCleaner fileCleaner;
    //正确SQL
    public static final String CORRECT_SQL = "CORRECT_SQL";
    //数据库基本数据
    public static final String TEST_SQL_DATA = "TEST_SQL_DATA";

    /**
     * 异步判题
     *
     * @param commitJudge {@link CommitJudge}
     * @param callback {@link Consumer}
     */
    @Async("judgeExecutor")
    public Future<Solution> judge(CommitJudge commitJudge, Consumer<Solution> callback) {
        //    public Solution(String solutionId, Integer language, int type,Map<String,String> testData) {
        Solution solution = new Solution(UUID.randomUUID().toString(), commitJudge.getLanguage().getNumber(),commitJudge.getCommitType().getNumber(),commitJudge.getTestDataMapMap(),commitJudge.getSourceCode());
        Solution resultSolution = null;
        try {
            if (solution.getType() == 0) {
                //编程题
                resultSolution = judgement.judge(solution);
            } else {
                //编程题
                solution.setCorrectSQL(solution.getTestData().get(CORRECT_SQL));
                resultSolution = sqlJudgement.judge(solution);
            }
            log.info("Judged: solution({})).", solution.getSolutionId());
        } catch (Exception e) {
            log.error("运行错误",e);
            solution.setState(SolutionState.JUDGED);
            solution.setResult(SolutionResult.IE);
        } finally {
            callback.accept(resultSolution);
//            fileCleaner.deleteTempFile(solution.getSolutionId());
        }
        return new AsyncResult<Solution>(resultSolution);
    }


    /**
     * 处理编程题测试数据
     */
    public void processSQLTestData(Map<String,String> testData){

    }
}
