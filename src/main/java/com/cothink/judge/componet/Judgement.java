package com.cothink.judge.componet;

import com.cothink.judge.dao.*;
import com.cothink.judge.error.UnsupportedLanguageError;
import com.cothink.judge.model.Runtime;
import com.cothink.judge.model.*;
import com.cothink.judge.type.Language;
import com.cothink.judge.type.SolutionResult;
import com.cothink.judge.type.SolutionState;
import com.cothink.judge.utils.FileWriteUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ThreadPoolExecutor;

@Slf4j
@Component
public class Judgement {
    private static final Integer OUTPUT_LIMIT = 16;
    private static final Integer MAX_MEM_LIMIT = Integer.MAX_VALUE;
    private static final String INPUT_FILE = ".in";
    private static final String OUTPUT_FILE = ".out";
    @Value("${project.file-dir}")
    private String fileDir;

    @Value("${project.code-dir}")
    private String codeDir;
    @Value("${project.judge-runner}")
    private String judgeRunnerDir;

    @Resource
    private ObjectMapper objectMapper;


    @Resource
    private DatabaseConfig dbConfig;

    @Resource
    private Compiler compiler;
    @Resource
    private FileWriteUtil fileWriteUtil;
    private static class RuntimeError extends Exception {
        RuntimeError(String msg) {
            super(msg);
        }
    }

    @PostConstruct
    private void init() {
        if (!codeDir.endsWith("/")) {
            codeDir += '/';
        }
        if (!fileDir.endsWith("/")) {
            fileDir += '/';
        }
    }

    /**
     * 判题入口
     * <p>隔离级别：读提交</p>
     *
     * @param solution {@link Solution}
     */
//    @Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    public Solution judge(Solution solution) {
        log.info("Judging: solution({}).", solution.getSolutionId());
        // 为当前事务禁用外键约束
//        dbConfig.disableFKChecks();

        Compile compile = compiler.compile(solution);

        if (compile.getState() == 0) {
            //修改内存和时间限制
            Limit limit = new Limit(Long.MAX_VALUE,Integer.MAX_VALUE);
            Runtime runtime = new Runtime(solution.getSolutionId());
            RunResult result = execute(solution, runtime, limit);
            log.info("result:{}",result);
            solution.setRunResult(result);
        } else {
            solution.setResult(SolutionResult.CE);
        }
        solution.setState(SolutionState.JUDGED);
        return solution;
    }

    /**
     * 保存结果
     *
     * @param result {@link RunResult}
     */
    private void saveResult(Solution solution, Runtime runtime, RunResult result) {
        if (runtime.getResult() == SolutionResult.IE || runtime.getResult() == SolutionResult.RE) {
            solution.setResult(runtime.getResult());
        } else {
            runtime.setTotal(result.getTotal());
            runtime.setPassed(result.getPassed());
            runtime.setTime(result.getTime());
            runtime.setMemory(result.getMemory());
            solution.setResult(SolutionResult.getByString(result.getResult()));
            Double passRate = result.getPassRate();
            solution.setPassRate(Double.isNaN(passRate) ? 0 : passRate);
        }

        solution.setState(SolutionState.JUDGED);
    }

    /**
     * 执行用户程序
     *
     * @return 运行结果 {@link RunResult}
     */
    private RunResult execute(Solution solution, Runtime runtime, Limit limit) {
        RunResult result = null;
        //测试数据目录 : fileDir/test_data/solution.solutionId()
        try {
            String testDataDir = fileDir + "test_data/" + solution.getSolutionId();
            processProgramTestData(solution.getTestData(),testDataDir);
            ProcessBuilder cmd = buildCommand(solution, limit, testDataDir);
            result = run(cmd);
        } catch (RuntimeError e) {
            log.error("Runtime Error:",e);
            runtime.setInfo(e.getMessage());
            runtime.setResult(SolutionResult.RE);
        } catch (InterruptedException | IOException | UnsupportedLanguageError e) {
            log.error("JudgeError",e);
            runtime.setResult(SolutionResult.IE);
            runtime.setInfo(e.getMessage());
        }

        return result;
    }

    /**
     * 调用判题程序执行
     *
     * @param cmd 命令 & 参数
     * @return 运行结果 {@link RunResult}
     */
    private RunResult run(ProcessBuilder cmd)
            throws RuntimeError, IOException, InterruptedException {
        RunResult result;
        Process process = cmd.start();

        int exitValue = process.waitFor();

        if (exitValue == 0) {
            // 正常退出
            String resultStr = IOUtils.toString(process.getInputStream());
            log.info("resultStr:{}",resultStr);
            result = objectMapper.readValue(resultStr, RunResult.class);
            log.info("result:{}",result);
        } else {
            // 非正常退出
            String stderr = IOUtils.toString(process.getErrorStream());
            if (exitValue == 1) {
                throw new RuntimeError(stderr);
            } else {
                throw new InterruptedException(stderr);
            }
        }

        process.destroy();
        return result;
    }

    /**
     * 生成命令
     */
    private ProcessBuilder buildCommand(Solution solution, Limit limit, String testDataDir)
            throws UnsupportedLanguageError {
        Language language = Language.get(solution.getLanguage());

        if (language == null) {
            throw new UnsupportedLanguageError("Unsupported language: null.");
        }

        String solutionDir = codeDir + solution.getSolutionId();

        ProcessBuilder builder = new ProcessBuilder();

        List<String> cmd = new ArrayList<>();
//        String path = System.getProperty("user.dir");
        log.info("judgeRunnerDir:{}",judgeRunnerDir);
        cmd.add(judgeRunnerDir);

        long timeLimit = limit.getTimeout();
        int memoryLimit = limit.getMemoryLimit();
        int maxMemoryLimit = memoryLimit << 2;
        if(maxMemoryLimit <0 ){
            maxMemoryLimit = memoryLimit;
        }
        int procLimit = 1;

        // Java/Kotlin/JS 内存限制按 2 倍计算
        switch (language) {
            case C:
            case CPP:
            case GO:
                procLimit = 10;
                maxMemoryLimit <<= 1;
                if(maxMemoryLimit < 0){
                    maxMemoryLimit = Integer.MAX_VALUE;
                }
                cmd.add("./Solution");
                break;
            case JAVA:
                memoryLimit <<= 1;
                if(memoryLimit < 0){
                    memoryLimit = Integer.MAX_VALUE;
                }
                maxMemoryLimit = memoryLimit << 2;
                if(maxMemoryLimit < 0){
                    maxMemoryLimit = Integer.MAX_VALUE;
                }
                cmd.add(String.format("java@-Xmx%dm@Solution", memoryLimit));
                break;
            case KOTLIN:
                timeLimit <<= 1;
                memoryLimit <<= 1;
                if(memoryLimit < 0){
                    memoryLimit = Integer.MAX_VALUE;
                }
                maxMemoryLimit = memoryLimit << 2;
                if(maxMemoryLimit < 0){
                    maxMemoryLimit =Integer.MAX_VALUE;
                }
                cmd.add("kotlin@SolutionKt");
                break;
            case JAVA_SCRIPT:
                memoryLimit <<= 1;
                if(memoryLimit < 0){
                    memoryLimit = Integer.MAX_VALUE;
                }
                cmd.add("node@Solution.js");
                break;
            case PYTHON:
                cmd.add("python3@Solution.py");
                break;
            case BASH:
                cmd.add("sh@Solution.sh");
                break;
            case C_SHARP:
                procLimit = 3;
                memoryLimit <<= 1;
                if(memoryLimit < 0){
                    memoryLimit =Integer.MAX_VALUE;
                }
                cmd.add("mono@Solution.exe");
                break;
            default:
                throw new UnsupportedLanguageError(String.format("Unsupported language: %s.", language));
        }

        if (maxMemoryLimit >= MAX_MEM_LIMIT) {
            maxMemoryLimit = MAX_MEM_LIMIT;
        }
        log.info("timeLimit:{},memoryLimit:{},maxMemoryLimit:{}",timeLimit,memoryLimit,maxMemoryLimit);
        List<String> config = Arrays.asList(
                Long.toString(timeLimit),
                Integer.toString(memoryLimit),
                Integer.toString(maxMemoryLimit),
                OUTPUT_LIMIT.toString(),
                solutionDir,
                testDataDir,
                Integer.toString(procLimit)
        );
        log.info("cmd:{}",cmd);
        cmd.addAll(config);
        log.info("cmd:{}",cmd);
        builder.command(cmd);
        return builder;
    }
    /**
     * 处理编程题的测试数据
     */
    public void processProgramTestData(Map<String,String> testData,String testDataDir) throws IOException {
        int number = 0;
        File fahterfile = new File(testDataDir);
        log.info("file:{}",fahterfile);
        fahterfile.mkdir();//创建文件夹
        fahterfile.setWritable(true,false);
        for(Map.Entry<String,String> entry : testData.entrySet()){
            if(testData.entrySet().size() == 1 && "".equals(entry.getKey())){
                //无输入的情况仅仅输出out
                String fileName = number + OUTPUT_FILE;
                File file = new File(testDataDir+File.separator+fileName);
                file.createNewFile();
                fileWriteUtil.write(file,entry.getValue());
                break;
            }
            String inFileName = number  + INPUT_FILE;
            String outFileName = number + OUTPUT_FILE;
            File inFile = new File(testDataDir + File.separator + inFileName);
            inFile.createNewFile();
            File outFile = new File(testDataDir + File.separator + outFileName);
            outFile.createNewFile();
            fileWriteUtil.write(inFile,entry.getKey());
            fileWriteUtil.write(outFile,entry.getValue());
            number ++;
        }
    }
//    @PostConstruct
    public void test() throws IOException {
        Map<String,String> data = new HashMap<>();
        data.put("","helloword");
        log.info("测试路径创建");
        processProgramTestData(data,"/Users/admin/code/test");
    }
}
