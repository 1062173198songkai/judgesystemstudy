package com.cothink.judge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class JudgeApplication {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(JudgeApplication.class, args);

    }

}
