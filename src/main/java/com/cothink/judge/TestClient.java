package com.cothink.judge;

import com.cothink.judge.grpc.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class TestClient {

    private final ManagedChannel channel;
    //阻塞调用
    public final OnlineJudgeGrpc.OnlineJudgeBlockingStub blockingStub;

    public TestClient(String host,int port){
        this(ManagedChannelBuilder.forAddress(host, port).usePlaintext(true).build());
    }
    TestClient(ManagedChannel channel){
        this.channel = channel;
        blockingStub = OnlineJudgeGrpc.newBlockingStub(channel);
    }

    public static void main(String[] arg0s){
        String host = "82.156.37.35";
        int port = 10920;
        TestClient client = new TestClient(host,port);
        String source = "public class Solution {\n" +
                "    public static void main(String[] args){\n" +
                "        System.out.println(\"hello world\");\n" +
                "    }\n" +
                "}\n";
        Language language = Language.JAVA;
        RunLimit runLimit = RunLimit.newBuilder().setMemoryLimit(Integer.MAX_VALUE).setTimeoutLimit(Integer.MAX_VALUE).build();
        CommitType commitType = CommitType.PROGRAM;
        Map<String,String> testData = new HashMap<String,String>();
        testData.put("","hello world");

        CommitJudge commitJudge = CommitJudge.newBuilder().setSourceCode(source).setLanguage(language).setLimit(runLimit).setLanguage(language)
                .setCommitType(commitType).putAllTestDataMap(testData).build();
        JudgeMessage judgeMessage = null;
        try {
            judgeMessage = client.blockingStub.sendJudge(commitJudge);
            log.info("judgeMessage:{}",judgeMessage);
        }catch (Exception e){
            log.error("异常",e);
        }
    }
}
